package config

import (
	"fmt"

	httpex "gitlab.com/saintmaur/lib/http"

	"github.com/spf13/viper"
	"gitlab.com/saintmaur/lib/logger"
)

var (
	GeneralConfig = new(Config)
)

func InitConfig(fileName string) bool {
	viper.SetConfigFile(fileName)
	fmt.Printf("Try to setup using the file: %s\n", viper.ConfigFileUsed())
	if err := viper.ReadInConfig(); err != nil {
		fmt.Printf("Failed on reading the config file (%s): %s\n", viper.ConfigFileUsed(), err)
		return false
	}
	if err := viper.Unmarshal(&GeneralConfig); err != nil {
		fmt.Println("Failed to unmarshal config: ", err)
		return false
	}
	return true
}

type WSConfig struct {
	Type           string `mapstructure:"type"`
	MessageTimeout string `mapstructure:"no_message_timeout"`
	VisitTimeout   string `mapstructure:"no_visit_timeout"`
	BaseURL        string `mapstructure:"base_url"`
	Limit          int    `mapstructure:"limit"`
}

type ConnectorConfig struct {
	Disabled bool                `mapstructure:"disabled"`
	WS       WSConfig            `mapstructure:"ws"`
	HTTP     httpex.ServerConfig `mapstructure:"http"`
}

// Config is a highest level config type
type Config struct {
	Logger     logger.LoggerConfig `mapstructure:"logger"`
	Connectors []ConnectorConfig   `mapstructure:"connectors"`
	Stat       httpex.ServerConfig `mapstructure:"stat"`
}

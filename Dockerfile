FROM golang:alpine as build
RUN which go
RUN apk add make git g++ libc-dev
ENV INSTALL_DIR=/go/src/ws-http-proxy/
WORKDIR ${INSTALL_DIR}
COPY . ${INSTALL_DIR}
RUN make build
FROM alpine:latest as final
ENV INSTALL_DIR=/go/src/ws-http-proxy/
WORKDIR /etc/ws-http-proxy
COPY --from=build ${INSTALL_DIR}/ws-http-proxy .
package main

import (
	"flag"
	"fmt"
	"os"
	"os/signal"
	"syscall"
	"time"

	"gitlab.com/saintmaur/ws-http-proxy/config"

	httpex "gitlab.com/saintmaur/lib/http"
	"gitlab.com/saintmaur/lib/logger"
	"gitlab.com/saintmaur/lib/stat"
	"gitlab.com/saintmaur/ws-http-proxy/connector"
)

var (
	version   string
	commit    string
	buildTime string
	appName   string
)

func main() {
	var configPath = flag.String("config", "", "the config path")
	flag.Parse()
	if !config.InitConfig(*configPath) {
		fmt.Println("Exit")
		return
	}
	defer logger.Stop()
	if !logger.InitLogger(config.GeneralConfig.Logger.Filename,
		config.GeneralConfig.Logger.Level,
		config.GeneralConfig.Logger.FormatJSON) {
		fmt.Println("Failed to init logger")
		return
	}
	logger.Info("Initialization complete")
	logger.Info("Starting the application")
	var connectors []*connector.Connector
	srv := httpex.New(&config.GeneralConfig.Stat)
	if srv == nil {
		logger.Info("Failed on creating a metrics server. Stopping")
		stat.Stop()
		os.Exit(1)
	}
	srv.Run()
	statC := make(stat.Chan, 1000)
	defer close(statC)
	stat.Run(statC)
	for _, serverConfig := range config.GeneralConfig.Connectors {
		if !serverConfig.Disabled {
			if s := connector.New(
				&serverConfig,
				statC,
			); s != nil {
				s.Run()
				connectors = append(connectors, s)
			}
		}
	}
	connectorsCount := len(connectors)
	if connectorsCount == 0 {
		logger.Info("No connector has been started. Stopping.")
		stat.Stop()
		os.Exit(1)
	}
	logger.Infof("All configurable connectors (%d) have been started.", connectorsCount)
	sigs := make(chan os.Signal, connectorsCount)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM)
	<-sigs
	logger.Info("A signal has been received. Stopping.")
	start := time.Now()
	for _, c := range connectors {
		logger.Info("Stop the connector")
		c.Stop()
		logger.Info("Stopped the connector")
	}
	stat.Stop()
	logger.Infof("Stopped for %s", time.Since(start))
}

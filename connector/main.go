package connector

import (
	"net/http"
	"sync"
	"time"

	httpex "gitlab.com/saintmaur/lib/http"
	"gitlab.com/saintmaur/lib/logger"
	"gitlab.com/saintmaur/lib/stat"
	"gitlab.com/saintmaur/ws-http-proxy/config"
	"gitlab.com/saintmaur/ws-http-proxy/ws"
)

type Connector struct {
	httpServer *httpex.Server
	wsManager  *ws.Manager
	stopC      chan struct{}
	statC      stat.Chan
	wg         sync.WaitGroup
	config     *config.ConnectorConfig
}

func (c *Connector) Stop() {
	c.stopC <- struct{}{}
	c.wg.Wait()
}

func New(serverConfig *config.ConnectorConfig, statC stat.Chan) *Connector {
	c := new(Connector)
	c.stopC = make(chan struct{})
	c.statC = statC
	c.config = serverConfig
	c.wsManager = ws.NewManager()
	c.httpServer = httpex.New(&serverConfig.HTTP)
	c.httpServer.AddHandler("/", func(w http.ResponseWriter, r *http.Request) {
		if c.wsManager.Len() >= c.config.WS.Limit {
			logger.Debug("Connections count is exceeded. Try to stop the oldest one")
			c.wsManager.StopOldest()
		}
		wsc, code := c.wsManager.Get(c.config.WS, r.URL.Query(), true)
		var data []byte
		if wsc != nil {
			data = wsc.GetData()
		} else {
			logger.Errorf("Can't get data for response: connection is nil")
		}
		w.WriteHeader(code)
		w.Write(data)
	})
	c.httpServer.Run()
	return c
}

func (c *Connector) Run() {
	c.wg.Add(1)
	go func() {
		for {
			select {
			case <-time.After(time.Second):
				c.statC <- stat.Rec{
					Name:        "ws_pool_size",
					Value:       float64(c.wsManager.Len()),
					Labels:      []string{"type"},
					LabelValues: []string{c.config.WS.Type},
				}
			case <-c.stopC:
				c.wsManager.Stop()
				c.httpServer.Stop()
				c.wg.Done()
				return
			}
		}
	}()
}

package ws

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"time"

	"github.com/google/uuid"
	"github.com/gorilla/websocket"
	"github.com/tidwall/gjson"
	"github.com/tidwall/sjson"
	"gitlab.com/saintmaur/lib/logger"
	"gitlab.com/saintmaur/ws-http-proxy/utils"
)

const (
	messageTypeWelcome = "welcome"
	messageTypeMessage = "message"
	messageTypePong    = "pong"
	messageTypeError   = "error"

	pathsType = "type"
)

type KucoinConnectionInitialConfig struct {
	Code string `json:"code"`
	Data struct {
		Token           string `json:"token"`
		InstanceServers []struct {
			Endpoint     string `json:"endpoint"`
			Encrypt      bool   `json:"encrypt"`
			Protocol     string `json:"protocol"`
			PingInterval int    `json:"pingInterval"`
			PingTimeout  int    `json:"pingTimeout"`
		} `json:"instanceServers"`
	} `json:"data"`
}

type KucoinWSConnection struct {
	config       KucoinConnectionInitialConfig
	pair         string
	connectionId string
	pingerStopC  chan struct{}
	conn         *WSConnectionData
	tmr          *time.Timer
	sequenceID   int64
}

func (c *KucoinWSConnection) deinit() {
	logger.WithField("ws", c.Name()).Info("Stop the pinger")
	c.pingerStopC <- struct{}{}
}

func (c *KucoinWSConnection) init(data string) (bool, int) {
	logger.WithField("ws", c.Name()).Infof("Init connection: %s", data)
	c.pair = data
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*3)
	defer cancel()
	req, err := http.NewRequestWithContext(ctx, "POST", c.conn.cfg.BaseURL+"/api/v1/bullet-public", nil)
	if err != nil {
		logger.WithField("ws", c.Name()).Errorf("Failed on creating an HTTP request: %s", err)
		return false, http.StatusInternalServerError
	}
	client := http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		logger.WithField("ws", c.Name()).Errorf("Failed on making an HTTP request: %s", err)
		return false, http.StatusInternalServerError
	}
	defer resp.Body.Close()
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		logger.WithField("ws", c.Name()).Errorf("Failed on reading the body: %s", err)
		return false, http.StatusInternalServerError
	}
	err = json.Unmarshal(body, &c.config)
	if err != nil {
		logger.WithField("ws", c.Name()).Errorf("Failed on unmarshalling the config: %s", err)
		return false, http.StatusInternalServerError
	}
	if c.config.Code != "200000" {
		logger.WithField("ws", c.Name()).Errorf("The response code is unsuccessful: %s", c.config.Code)
		return false, resp.StatusCode
	}
	c.connectionId = uuid.New().String()
	wsURL := fmt.Sprintf("%s?token=%s&connectId=%s", c.config.Data.InstanceServers[0].Endpoint, c.config.Data.Token, c.connectionId)
	c.conn.conn = newWS(wsURL)
	if c.conn.conn == nil {
		logger.WithField("ws", c.Name()).Errorf("Failed on creating a WS connection")
		return false, http.StatusInternalServerError
	}
	ctx, cancel = context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()
	orderbookURL := fmt.Sprintf("%s/api/v1/market/orderbook/level2_100?symbol=%s&limit=1000", c.conn.cfg.BaseURL, c.pair)
	req, err = http.NewRequestWithContext(ctx, "GET", orderbookURL, nil)
	if err != nil {
		logger.WithField("ws", c.Name()).Errorf("Failed on creating an HTTP request: %s", err)
		return false, http.StatusInternalServerError
	}
	resp, err = client.Do(req)
	if err != nil {
		logger.WithField("ws", c.Name()).Errorf("Failed on making an HTTP request: %s", err)
		return false, http.StatusInternalServerError
	}
	defer resp.Body.Close()
	if resp.StatusCode != 200 {
		logger.WithField("ws", c.Name()).Errorf("Got status code %d on HTTP request '%s'", resp.StatusCode, orderbookURL)
		return false, resp.StatusCode
	}
	c.conn.buffer, err = io.ReadAll(resp.Body)
	if err != nil {
		logger.WithField("ws", c.Name()).Errorf("Failed on reading the body: %s", err)
		return false, http.StatusInternalServerError
	}
	rawSequinceID := gjson.GetBytes(c.conn.buffer, "data.sequence")
	if rawSequinceID.Value() == nil {
		logger.WithField("ws", c.Name()).Error("Failed on verifying the orderbook data: sequence ID is nil")
		return false, http.StatusNotFound
	}
	c.sequenceID = rawSequinceID.Int()
	return true, http.StatusOK
}

func (c *KucoinWSConnection) GetData() []byte {
	return c.conn.Buffer()
}
func (c *KucoinWSConnection) GetQueueSize() int {
	return len(c.conn.dataC)
}
func (c *KucoinWSConnection) process(message []byte) {
	result := gjson.GetBytes(message, pathsType)
	switch result.String() {
	case messageTypeWelcome:
		c.conn.outgoingC <- []byte(fmt.Sprintf(`{"id": "%s","type": "subscribe","topic": "/market/level2:%s"}`, c.connectionId, c.Name()))
		c.startPinger()
	case messageTypeMessage:
		c.processMessage(message)
	case messageTypePong:
		c.tmr.Stop()
	case messageTypeError:
		logger.WithField("ws", c.Name()).Warnf("Got an error from channel: %s", message)
	default:
		logger.WithField("ws", c.Name()).Infof("Unknown message type: %s", string(message))
	}
}

func (c *KucoinWSConnection) startPinger() {
	pingerStopC := make(chan struct{})
	pingInterval, _ := time.ParseDuration(fmt.Sprintf("%dms", c.config.Data.InstanceServers[0].PingInterval))
	pingTimeout, _ := time.ParseDuration(fmt.Sprintf("%dms", c.config.Data.InstanceServers[0].PingTimeout))
	c.pingerStopC = pingerStopC
	go func() {
		for {
			select {
			case <-c.pingerStopC:
				if c.tmr != nil {
					c.tmr.Stop()
				}
				return
			case <-time.After(pingInterval):
				c.outgoingChannel() <- []byte(fmt.Sprintf(`{"id": "%s","type": "ping"}`, c.connectionId))
				c.startOrResetTimer(pingTimeout)
			}
		}
	}()
}

func (c *KucoinWSConnection) startOrResetTimer(pingTimeout time.Duration) {
	if c.tmr == nil {
		c.tmr = time.AfterFunc(pingTimeout, func() {
			logger.WithField("ws", c.Name()).Info("Deinit the connection")
			c.Stop()
		})
	} else {
		c.tmr.Stop()
		c.tmr.Reset(pingTimeout)
	}
}
func (c *KucoinWSConnection) updatePricesSequentially(message []byte) {
	start := gjson.GetBytes(message, "data.sequenceStart").Int()
	end := gjson.GetBytes(message, "data.sequenceEnd").Int()
	askChanges := gjson.GetBytes(message, "data.changes.asks").Array()
	bidChanges := gjson.GetBytes(message, "data.changes.bids").Array()
	var err error
	timestamp := gjson.GetBytes(message, "data.time").Int()
	c.conn.buffer, err = sjson.SetBytes(c.conn.buffer, "data.time", timestamp)
	if err != nil {
		logger.WithField("ws", c.Name()).Errorf("Failed on updating the timestamp: %s", err)
		return
	}
	for i := start; i <= end; i++ {
		for _, chng := range askChanges {
			if chng.Array()[2].Int() == i {
				c.updatePrice(&chng, "asks", utils.Less)
			}
		}
		for _, chng := range bidChanges {
			if chng.Array()[2].Int() == i {
				c.updatePrice(&chng, "bids", utils.More)
			}
		}
	}
}

func (c *KucoinWSConnection) updatePrice(chng *gjson.Result, tp string, cmp func(float64, float64) bool) {
	newPrice := chng.Array()[0]
	amount := chng.Array()[1]
	sequenceID := chng.Array()[2].Int()
	if c.sequenceID >= sequenceID {
		logger.WithField("ws", c.Name()).Infof("[%d] Skip: %d", c.sequenceID, sequenceID)
		return
	}
	c.sequenceID = sequenceID
	var err error
	c.conn.buffer, err = sjson.SetBytes(c.conn.buffer, "data.sequence", fmt.Sprintf(`%d`, c.sequenceID))
	if err != nil {
		logger.WithField("ws", c.Name()).Warnf("Failed on setting the sequence ID: %s", err)
		return
	}
	if newPrice.Float() == 0 {
		return
	}
	tpPrices := gjson.GetBytes(c.conn.buffer, fmt.Sprintf("data.%s.#.0", tp)).Array()
	if amount.Float() == 0 {
		for i, price := range tpPrices {
			if price.Float() == newPrice.Float() {
				var err error
				c.conn.buffer, err = sjson.DeleteBytes(c.conn.buffer, fmt.Sprintf("data.%s.%d", tp, i))
				if err != nil {
					logger.WithField("ws", c.Name()).Warnf("Failed on deleting the price: %s", err)
				}
				break
			}
		}
	} else {
		for i, price := range tpPrices {
			if price.Float() == newPrice.Float() {
				c.conn.buffer, err = sjson.SetBytes(c.conn.buffer, fmt.Sprintf("data.%s.%d.1", tp, i), amount.String())
				if err != nil {
					logger.WithField("ws", c.Name()).Warnf("Failed on updating the price: %s", err)
					return
				}
				break
			}
			if cmp(newPrice.Float(), price.Float()) {
				prices := gjson.GetBytes(c.conn.buffer, fmt.Sprintf("data.%s", tp)).Array()
				var arr [][]string
				for j, value := range prices {
					if j == i {
						arr = append(arr, []string{newPrice.String(), amount.String()})
					}
					arr = append(arr, []string{value.Array()[0].String(), value.Array()[1].String()})
				}
				c.conn.buffer, err = sjson.SetBytes(c.conn.buffer, fmt.Sprintf("data.%s", tp), arr)
				if err != nil {
					logger.WithField("ws", c.Name()).Warnf("Failed on inserting the price: %s", err)
				}
				break
			} else {
				if i == len(tpPrices)-1 {
					c.conn.buffer, err = sjson.SetBytes(c.conn.buffer, fmt.Sprintf("data.%s.-1", tp), []string{newPrice.String(), amount.String()})
					if err != nil {
						logger.WithField("ws", c.Name()).Warnf("Failed on inserting the price: %s", err)
					}
				}
			}
		}
	}
}

func (c *KucoinWSConnection) processMessage(message []byte) {
	c.conn.m.Lock()
	defer c.conn.m.Unlock()
	logger.WithField("ws", c.Name()).Debugf("New message: %s", message)
	c.updatePricesSequentially(message)
}
func (c *KucoinWSConnection) Stop() {
	c.conn.stopC <- struct{}{}
}
func (c *KucoinWSConnection) stopChannel() chan struct{} {
	return c.conn.stopC
}
func (c *KucoinWSConnection) dataChannel() chan []byte {
	return c.conn.dataC
}
func (c *KucoinWSConnection) outgoingChannel() chan []byte {
	return c.conn.outgoingC
}
func (c *KucoinWSConnection) connection() *websocket.Conn {
	return c.conn.conn
}
func (c *KucoinWSConnection) Name() string {
	return c.conn.name
}
func (c *KucoinWSConnection) Type() string {
	return c.conn.cfg.Type
}
func (c *KucoinWSConnection) StartTime() time.Time {
	return c.conn.startTime
}
func (c *KucoinWSConnection) resetMessageTimeout() {
	c.conn.resetMessageTimeout()
}
func (c *KucoinWSConnection) resetVisitTimeout() {
	c.conn.resetVisitTimeout()
}
func (c *KucoinWSConnection) getDuration() time.Duration {
	return time.Since(c.conn.startTime)
}

package ws

import (
	"net/http"
	"time"

	"github.com/gorilla/websocket"
	"gitlab.com/saintmaur/lib/logger"
)

type SimpleWSConnection struct {
	conn *WSConnectionData
}

func (c *SimpleWSConnection) init(data string) (bool, int) {
	c.conn.conn = newWS(c.conn.cfg.BaseURL)
	if c.conn == nil {
		logger.Errorf("Failed on creating a WS connection")
		return false, http.StatusInternalServerError
	}
	return true, http.StatusOK
}
func (c *SimpleWSConnection) deinit() {
	logger.WithField("ws", c.Name()).Info("Stop it all")
}

func (c *SimpleWSConnection) GetData() []byte {
	return c.conn.Buffer()
}
func (c *SimpleWSConnection) GetQueueSize() int {
	return len(c.conn.dataC)
}
func (c *SimpleWSConnection) process(message []byte) {
	logger.Infof("Got a new message: %s", message)
	c.conn.buffer = message
}
func (c *SimpleWSConnection) Stop() {
	c.conn.stopC <- struct{}{}
}
func (c *SimpleWSConnection) stopChannel() chan struct{} {
	return c.conn.stopC
}
func (c *SimpleWSConnection) dataChannel() chan []byte {
	return c.conn.dataC
}
func (c *SimpleWSConnection) outgoingChannel() chan []byte {
	return c.conn.outgoingC
}
func (c *SimpleWSConnection) connection() *websocket.Conn {
	return c.conn.conn
}
func (c *SimpleWSConnection) Name() string {
	return c.conn.name
}
func (c *SimpleWSConnection) getDuration() time.Duration {
	return time.Since(c.conn.startTime)
}
func (c *SimpleWSConnection) Type() string {
	return c.conn.cfg.Type
}
func (c *SimpleWSConnection) StartTime() time.Time {
	return c.conn.startTime
}
func (c *SimpleWSConnection) resetMessageTimeout() {
	c.conn.resetMessageTimeout()
}
func (c *SimpleWSConnection) resetVisitTimeout() {
	c.conn.resetVisitTimeout()
}

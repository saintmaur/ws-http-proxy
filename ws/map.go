package ws

import "sync"

type connections_sync_map struct {
	m    sync.Mutex
	data map[string]WSConnectionI
}

func (csm *connections_sync_map) store(key string, value WSConnectionI) {
	csm.m.Lock()
	defer csm.m.Unlock()
	_, ok := csm.data[key]
	if !ok {
		csm.data[key] = value
	}
}

func (csm *connections_sync_map) load(key string) (WSConnectionI, bool) {
	csm.m.Lock()
	defer csm.m.Unlock()
	value, ok := csm.data[key]
	return value, ok
}

func (csm *connections_sync_map) delete(key string) {
	csm.m.Lock()
	defer csm.m.Unlock()
	_, ok := csm.data[key]
	if ok {
		delete(csm.data, key)
	}
}

func (csm *connections_sync_map) iterate(f func(WSConnectionI)) {
	csm.m.Lock()
	defer csm.m.Unlock()
	for _, c := range csm.data {
		f(c)
	}
}

func (csm *connections_sync_map) len() int {
	csm.m.Lock()
	l := len(csm.data)
	defer csm.m.Unlock()
	return l
}

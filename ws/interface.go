package ws

import (
	"sync"
	"time"

	"github.com/gorilla/websocket"
	"gitlab.com/saintmaur/lib/utils"
	"gitlab.com/saintmaur/ws-http-proxy/config"
)

type WSConnectionData struct {
	conn           *websocket.Conn
	name           string
	stopC          chan struct{}
	dataC          chan []byte
	outgoingC      chan []byte
	buffer         []byte
	m              sync.Mutex
	startTime      time.Time
	messageTimeout time.Duration
	visitTimeout   time.Duration
	messageTimer   *time.Timer
	visitTimer     *time.Timer
	cfg            config.WSConfig
}

func (wd *WSConnectionData) Buffer() []byte {
	wd.m.Lock()
	defer wd.m.Unlock()
	return wd.buffer
}

func (wd *WSConnectionData) resetMessageTimeout() {
	wd.m.Lock()
	defer wd.m.Unlock()
	wd.messageTimer.Reset(wd.messageTimeout)
}
func (wd *WSConnectionData) resetVisitTimeout() {
	wd.m.Lock()
	defer wd.m.Unlock()
	wd.visitTimer.Reset(wd.visitTimeout)
}

func newWSConnectionData(wsc WSConnectionI, cfg config.WSConfig, name string) *WSConnectionData {
	messageTimeout := utils.ParseDuration(cfg.MessageTimeout, time.Minute)
	visitTimeout := utils.ParseDuration(cfg.VisitTimeout, time.Minute)
	return &WSConnectionData{
		name:           name,
		stopC:          make(chan struct{}),
		outgoingC:      make(chan []byte, 1000),
		startTime:      time.Now(),
		messageTimeout: messageTimeout,
		visitTimeout:   visitTimeout,
		messageTimer:   time.AfterFunc(messageTimeout, func() { wsc.Stop() }),
		visitTimer:     time.AfterFunc(visitTimeout, func() { wsc.Stop() }),
		cfg:            cfg,
	}
}

type WSConnectionI interface {
	GetData() []byte
	GetQueueSize() int
	Name() string
	Type() string
	StartTime() time.Time
	Stop()

	init(string) (bool, int)
	process([]byte)
	deinit()

	stopChannel() chan struct{}
	dataChannel() chan []byte
	outgoingChannel() chan []byte

	connection() *websocket.Conn

	resetMessageTimeout()
	resetVisitTimeout()
	getDuration() time.Duration
}

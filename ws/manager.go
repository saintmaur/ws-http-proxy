package ws

import (
	"fmt"
	"net/url"
	"sync"
	"time"

	"github.com/gorilla/websocket"
	"gitlab.com/saintmaur/lib/logger"
	"gitlab.com/saintmaur/ws-http-proxy/config"
)

type Manager struct {
	connections connections_sync_map
	wg          sync.WaitGroup
}

func NewManager() *Manager {
	m := new(Manager)
	m.connections.data = make(map[string]WSConnectionI)
	return m
}

func (m *Manager) Len() int {
	return m.connections.len()
}

func (m *Manager) StopOldest() {
	minTime := time.Now()
	var c WSConnectionI
	m.connections.iterate(func(wsc WSConnectionI) {
		cTime := wsc.StartTime()
		logger.Debugf("Compare: %s with %s", cTime, minTime)
		if cTime.Compare(minTime) < 1 {
			c = wsc
		}
	})
	if c != nil {
		c.Stop()
	}
}

func (m *Manager) Get(cfg config.WSConfig, params url.Values, create bool) (WSConnectionI, int) {
	name := prepareNameByType(cfg.Type, params)
	fullName := fmt.Sprintf("%s_%s", cfg.Type, name)
	c, ok := m.connections.load(fullName)
	if ok {
		c.resetVisitTimeout()
		return c, 200
	}
	if !create {
		return nil, 200
	}
	logger.Infof("Couldn't find a connection named %s. Create new", fullName)
	switch cfg.Type {
	case types_kucoin:
		wsc := new(KucoinWSConnection)
		wsc.conn = newWSConnectionData(wsc, cfg, name)
		c = wsc
	case types_simple:
		wsc := new(SimpleWSConnection)
		wsc.conn = newWSConnectionData(wsc, cfg, name)
		c = wsc
	default:
		wsc := new(KucoinWSConnection)
		wsc.conn = newWSConnectionData(wsc, cfg, name)
		c = wsc
	}
	ok, code := c.init(name)
	if !ok {
		logger.Infof("Failed on initializing a connection named %s. Destroy it", fullName)
		closeConnection(c.connection())
		return nil, code
	}
	m.connections.store(fullName, c)
	m.run(c)
	return c, 200
}

// TODO: it is a subject to be moved down to each implementation of WSConnectionI
func prepareNameByType(tp string, params url.Values) string {
	result := ""
	switch tp {
	case types_kucoin:
		result = params.Get("symbol")
	case types_simple:
		result = "simple"
	default:
		result = params.Get("symbol")
	}
	return result
}

func (m *Manager) Stop() {
	m.connections.iterate(func(wsc WSConnectionI) {
		wsc.Stop()
	})
}

func (m *Manager) run(c WSConnectionI) {
	m.wg.Add(1)
	retryCount := 3
	go func() {
		logger.WithField("ws", c.Name()).Info("Enter the reader loop")
		defer logger.WithField("ws", c.Name()).Info("Exit the reader loop")
		var message []byte
		var err error
		for {
			if c.connection() != nil {
				_, message, err = c.connection().ReadMessage()
				if err != nil {
					logger.Infof("%s", err)
					retryCount--
					if retryCount > 0 {
						time.Sleep(time.Second)
						continue
					} else {
						m.wg.Done()
						return
					}
				}
				c.resetMessageTimeout()
				retryCount = 3
				logger.WithField("ws", c.Name()).WithField("ws", c.Name()).Debugf("Input: %s", message)
				c.process(message)
			} else {
				logger.Info("The connection is nil")
				m.wg.Done()
				return
			}
		}
	}()
	m.wg.Add(1)
	go func() {
		logger.WithField("ws", c.Name()).Info("Enter the controller loop")
		defer logger.WithField("ws", c.Name()).Info("Exit the controller loop")
		for {
			select {
			case <-c.stopChannel():
				logger.WithField("ws", c.Name()).Infof("Stop the connection after %s of work", c.getDuration())
				c.deinit()
				closeConnection(c.connection())
				m.connections.delete(fmt.Sprintf("%s_%s", c.Type(), c.Name()))
				m.wg.Done()
				return
			case message := <-c.outgoingChannel():
				logger.WithField("ws", c.Name()).Debugf("Output: %s", message)
				err := c.connection().WriteMessage(websocket.TextMessage, message)
				if err != nil {
					logger.
						WithField("ws", c.Name()).
						Warnf("Failed on sending a message to the websocket: %s", err)
				}
			}
		}
	}()
}

func closeConnection(conn *websocket.Conn) {
	if conn != nil {
		conn.
			WriteMessage(websocket.CloseMessage,
				websocket.FormatCloseMessage(websocket.CloseNormalClosure, ""))
		conn.Close()
	}
}

func newWS(url string) *websocket.Conn {
	d := websocket.Dialer{}
	conn, response, err := d.Dial(url, nil)
	if err != nil {
		logger.Warnf("Failed on dialing the '%s' with error: %s", url, err)
		return nil
	}
	if (response.StatusCode != 200) && (response.StatusCode != 101) {
		logger.Warnf("Failed on dialing the '%s' with status code: %d", url, response.StatusCode)
		return nil
	}
	return conn
}
